# MkDocs Pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/mkdocs?branch=main)](https://gitlab.com/buildgarden/pipelines/mkdocs/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/mkdocs)](https://gitlab.com/buildgarden/pipelines/mkdocs/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Build documentation sites with [MkDocs](https://www.mkdocs.org/).

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

### Project Setup

At a minimum, this pipeline requires that:

- A `mkdocs.yml` file is created

- A `docs/` directory exists and contains some Markdown files

### Using The Pipeline

Include the pipeline in the `.gitlab-ci.yml` file:

```yaml
include:
  - project: buildgarden/pipelines/mkdocs
    file: mkdocs-build.yml
```
